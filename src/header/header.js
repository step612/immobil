import React from 'react';
import logo from './logo.jpg';


export default function header(props) {

    return (
   
<div className="header">
  <div className="menu">
  <figure className="logo" >
            <img className="logo__elem" src={logo} alt="logo" width="100%" height="100%"/>
  
            </figure>
  </div>
  <div className="options">
       <select>
        <option value="casa">Casa</option>
        <option value="immobile">Immobile</option>
        <option value="stanza">Stanza</option>
        <option value="garage">Garage</option>
       </select>
  </div>
   <div className="scritta"><input class="holder" placeholder="inserisci indirizzo, CAP, o qualsiasi parola chiave"/></div>
  <div className="punto1"><h4 class="punto1__elem">Cerca</h4></div>
  <div className="punto2"> 
   <div className="punto2__elem">Ricerca avanzata
  </div>
  </div>
  <div className="menu--mod">
      <div className="menu__elem"></div>
      <div className="menu__elem"></div>
      <div className="menu__elem"></div>
   </div>
   </div>


    );
}
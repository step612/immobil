import React from 'react';

export default function aside () {

    return (

        
<aside>
  
  <div class="side">
    
    <div class="side__elem2">Mappa</div>
   
 <div class="side__elem1">
    <div class="side__elem3">
       
      <div class="menu--mod1">
      <div class="menu__elem1"></div>
      <div class="menu__elem1"></div>
      <div class="menu__elem1"></div>
   </div>
       
     </div>
      <div class="side__elem1--mod">Lista</div>
       </div>
 </div>
  
  <div class="contenitore">
   
    <div class="list">
      <div class="list__elem">Cerco <strong class="grassetto">%Casa%</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
      
    <div class="list">
      <div class="list__elem">in <strong class="grassetto">%Vendita%</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
  
    <div class="list">
      <div class="list__elem">a <strong class="grassetto">%Roma%</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
  
    <div class="list">
      <div class="list__elem">In zona <strong class="grassetto">%Prati%</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
  
    <div class="list">
      <div class="list__elem">Vicino <strong class="grassetto">Fermata della metro</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
    
    
    <div class="middle">Filtri</div>
    
  
     <div class="list">
      <div class="list__elem">Prezzo <em class="normal"> a partire da</em> <strong class="grassetto"> 175.000.000 €</strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
      
     <div class="list">
      <div class="list__elem">Mq <strong class="grassetto"></strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
  
     <div class="list">
      <div class="list__elem">Locali <strong class="grassetto"></strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
  
    <div class="list">
      <div class="list__elem">Bagni <strong class="grassetto"></strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
 
    <div class="list">
      <div class="list__elem">Altri filtri <strong class="grassetto"></strong>        </div>
      <div class="list__elem-mod">Modifica </div> 
    </div>
    </div>
  
</aside>


    )
}
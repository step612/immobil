import React from 'react';

import './App.css';
import './header/header.css';
import './grad/grad.css';
import './nav/nav.css';
import './breacker/breacker.css';
import './cards/cards.css';
import './aside/aside.css';
import './footer/footer.css';


import Header from './header/header';
import Grad from './grad/grad';
import Nav from './nav/nav';
import Breacker from './breacker/breacker';
import Cards from './cards/cards';
import Aside from './aside/aside';
import Footer from './footer/footer';

function App() {
  return (
    <body>
    <div className="App">
      <Header/>
      <Grad/>
      <Nav/>
      <Breacker/>
      <Cards/>
      <Aside/>
      <Footer/>
    </div>
    </body>
  );
}

export default App;
